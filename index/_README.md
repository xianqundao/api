# 首页接口文档

## 1. 轮播接口
### 1.1 功能描述
轮播（一般为4个）。
### 1.2 请求说明
> 请求方式：GET<br>
请求URL ：[/index/banner.json](#)<br>
完整(https://gitlab.com/xianqundao/api/-/raw/main/index/banner.json)

### 1.3 请求参数
无参数
### 1.4 返回结果
```json  
{
    "errorCode": 0,
    "errorMsg": "",
    "data": [
        {
            "id": 1,
            "name": "CCTV眼中的仙裙岛",
            "description": "CCTV发现之旅栏目的视频",
            "imageUrl": "http://r.photo.store.qq.com/psc?/V547xhag48w5IS4I7n9m0wxiY01v62Mj/45NBuzDIW489QBoVep5mcW97A7FK.SbXI3rc4o2l4Wzj2aiY09sX19zKOHDpADnmVIBM*GAWrV0KWqn5SR.QB.elE2jnvNk9s2aHksN5uvE!/r",
            "listImageUrl": "http://r.photo.store.qq.com/psc?/V547xhag48w5IS4I7n9m0wxiY01v62Mj/45NBuzDIW489QBoVep5mcZgeh3TSlG42Iv34euv8PDB95r9TU1GDBZXC2f0eU9v0EwJZAeouorW.AkYwgTVyDt0wIkS3ZxaAxW3*5KTxLXA!/r",
            "toUrl": "https://mp.weixin.qq.com/s/cFCzygQz7gCF7rS0oc_qrA"
        }
    ]
}
``` 
### 1.5 返回参数
字段              |字段类型|字段说明
------------|-----------|-----------
errorCode       |int    |错误码(0：成功)
errorMsg        |string |错误说明
data            |object |一般为对象
|               |
data            |       |
id              |int    |数据ID
name            |string |名称
description     |string |描述
imageUrl        |string |轮播图片
listImageUrl    |string |列表图片
toUrl           |string |详情页面
### 1.6 错误状态码
参见 [全局响应状态码说明](../introduction.html/#134-全局响应状态码说明)


---


## 2. 住宿汇总接口
### 2.1 功能描述
可以直接打电话的酒店（一般超过4个）。
### 2.2 请求说明
> 请求方式：GET<br>
请求URL ：[/index/hotel.json](#)<br>
完整(https://gitlab.com/xianqundao/api/-/raw/main/index/hotel.json)

### 2.3 请求参数
无参数
### 2.4 返回结果
```json  
{
    "errorCode": 0,
    "errorMsg": "",
    "data": [
        {
            "id": 1,
            "name": "港湾宾馆",
            "phoneNumber": "07597596008",
            "address": "广东省湛江市遂溪县江洪镇375省道陈玛配味店对面(近江洪镇中心幼儿园,江洪镇政府)",
            "imageUrl": "http://r.photo.store.qq.com/psc?/V547xhag48w5IS4I7n9m0wxiY02l3xK2/45NBuzDIW489QBoVep5mcYYQEn5kk8r4MNCdxBPDf1gDtrZmqNCbDFpNeDL8uL5arwaDuJgEWElLiCoaay56ARfGzn7Cuw1SknEgpIDffys!/r",
            "toUrl": "https://mp.weixin.qq.com/s/sk_bZRWZGsx7D26RBVUZWw"
        }
    ]
}
``` 
### 2.5 返回参数
字段              |字段类型|字段说明
------------|-----------|-----------
errorCode       |int    |错误码(0：成功)
errorMsg        |string |错误说明
data            |object |一般为对象
|        |
data       |        |
id              |int    |数据ID
name            |string |酒店名称
phoneNumber     |string |订房电话
address         |string |酒店地址
imageUrl        |string |酒店主图
toUrl           |string |汇总页面
### 2.6 错误状态码
参见 [全局响应状态码说明](../introduction.html/#134-全局响应状态码说明)


---


## 3. 仙裙美拍接口
### 3.1 功能描述
美拍（一般为4个）。
### 3.2 请求说明
> 请求方式：GET<br>
请求URL ：[/index/take.json](#)<br>
完整(https://gitlab.com/xianqundao/api/-/raw/main/index/take.json)

### 3.3 请求参数
无参数
### 3.4 返回结果
```json  
{
    "errorCode": 0,
    "errorMsg": "",
    "data": [
        {
            "id": 1,
            "name": "在江洪港无人机应该这样飞！",
            "description": "航拍高手这就给你露几手。",
            "author": "苏苏海味",
            "type": 2,
            "imageUrl": "http://r.photo.store.qq.com/psc?/V547xhag48w5IS4I7n9m0wxiY00vuOOb/45NBuzDIW489QBoVep5mccWOYGm5ZL.uUjNzHbO6j97DQS46HdIhAJMVhvBj5smdADs6ZJqxp5jc4q6r8PQ*w5v*pMdunL2TgyEa8FstbkY!/r",
            "toUrl": "https://mp.weixin.qq.com/s/wjcHnu2On5zW70YgPn4SeQ"
        }
    ]
}
``` 
### 3.5 返回参数
字段              |字段类型|字段说明
------------|-----------|-----------
errorCode       |int    |错误码(0：成功)
errorMsg        |string |错误说明
data            |object |一般为对象
|        |
data       |        |
id              |int    |数据ID
name            |string |标题
description     |string |描述
author          |string |作者
type            |string |类型(0=图文；1=音频；2=视频)
imageUrl        |string |美拍主图
toUrl           |string |详情页面
### 3.6 错误状态码
参见 [全局响应状态码说明](../introduction.html/#134-全局响应状态码说明)


---


## 4. 游玩攻略接口
### 4.1 功能描述
攻略（一般为5个）。
### 4.2 请求说明
> 请求方式：GET<br>
请求URL ：[/index/tip.json](#)<br>
完整(https://gitlab.com/xianqundao/api/-/raw/main/index/tip.json)

### 4.3 请求参数
无参数
### 4.4 返回结果
```json  
{
    "errorCode": 0,
    "errorMsg": "",
    "data": [
        {
            "id": 1,
            "name": "主播口中的仙裙岛",
            "description": "“离陆地最近”的海岛",
            "author": "黑揍红",
            "type": 1,
            "imageUrl": "http://r.photo.store.qq.com/psc?/V547xhag48w5IS4I7n9m0wxiY03csTbA/45NBuzDIW489QBoVep5mcRg27S4bh9JbIhr*XlxN2zfsJsILxh2.CSwuTSZrcUQlFDZl5vsuuYyMCo6nx.FWRbDJii9hgC6fqxx0jie80YM!/r",
            "toUrl": "https://mp.weixin.qq.com/s/hbDeImZKjBXV5hk7odUGmw"
        }
    ]
}
``` 
### 4.5 返回参数
字段              |字段类型|字段说明
------------|-----------|-----------
errorCode       |int    |错误码(0：成功)
errorMsg        |string |错误说明
data            |object |一般为对象
|        |
data       |        |
id              |int    |数据ID
name            |string |标题
description     |string |描述
author          |string |作者
type            |string |类型(0=图文；1=音频；2=视频)
imageUrl        |string |攻略主图
toUrl           |string |详情页面
### 4.6 错误状态码
参见 [全局响应状态码说明](../introduction.html/#134-全局响应状态码说明)