# 首页便民信息接口文档

## 1. 便民信息接口
### 1.1 功能描述
便民信息（一般为1个数据）。
### 1.2 请求说明
> 请求方式：GET<br>
请求URL ：[/index/convenient/1.json](#)<br>
完整(https://gitlab.com/xianqundao/api/-/raw/main/index/convenient/1.json)

### 1.3 请求参数
无参数
### 1.4 返回结果
```json  
{
    "errorCode": 0,
    "errorMsg": "",
    "data": {
    "month": 1,
    "list": [
      {
        "date": "2022年01月01日 周六",
        "sunrise": "07:18:10",
        "sunset": "18:11:09",
        "ID": "5403301482704339780",
        "RecordTime": "2021-09-06 12:56:14",
        "ReportID": "5578896256337264032",
        "Day": 1,
        "a0": 353,
        "a1": 397,
        "a2": 442,
        "a3": 477,
        "a4": 492,
        "a5": 483,
        "a6": 448,
        "a7": 393,
        "a8": 324,
        "a9": 252,
        "a10": 183,
        "a11": 127,
        "a12": 90,
        "a13": 76,
        "a14": 85,
        "a15": 112,
        "a16": 145,
        "a17": 177,
        "a18": 203,
        "a19": 221,
        "a20": 235,
        "a21": 250,
        "a22": 271,
        "a23": 300,
        "cs0": "04:08",
        "cg0": 492,
        "cs1": "13:04",
        "cg1": 76,
        "cs2": null,
        "cg2": null,
        "cs3": null,
        "cg3": null,
        "cs4": null,
        "cg4": null,
        "cs5": null,
        "cg5": null
      }
    ]
  }
}
``` 
### 1.5 返回参数
字段              |字段类型|字段说明
------------|-----------|-----------
errorCode       |int    |错误码(0：成功)
errorMsg        |string |错误说明
data            |object |一般为对象
|               |
data            ||
month           |int    |数据ID(从1开始，如：1为1月份)
list            |数组     |每天潮汐、日出日落
|               |
list...         ||
Day             |int    |第几天(当月)
sunrise         |string |日出时间
sunset          |string |日落时间
a0...           |string |00时潮汐高度
cs0...          |string |潮汐拐点时间
cg0...          |string |潮汐拐点高度
ID              |string |潮汐数据ID
RecordTime      |string |潮汐数据录入时间
ReportID        |string |潮汐数据发布ID
### 1.6 错误状态码
参见 [全局响应状态码说明](../introduction.html/#134-全局响应状态码说明)


---