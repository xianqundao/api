# 关于接口文档

## 1. 关于接口
### 1.1 功能描述
关于（仙裙简介、版权说明、联系客服）。
### 1.2 请求说明
> 请求方式：GET<br>
请求URL ：[/about/about.json](#)<br>
完整(https://gitlab.com/xianqundao/api/-/raw/main/about/about.json)

### 1.3 请求参数
无参数
### 1.4 返回结果
```json  
{
    "errorCode": 0,
    "errorMsg": "",
    "data": {
        "introduction": {
            "name": "仙裙简介",
            "imageUrl": "http://r.photo.store.qq.com/psc?/V547xhag48w5IS4I7n9m0wxiY00Dy7MR/45NBuzDIW489QBoVep5mcQOblVXuUfMib4Gy10bQ83RmzWdxeyJKfynOh8X5iayxxXgw14fwvxZ.INg7LKiwDWm5XcRPfUpuRrIGWFwfZ8o!/r",
            "toUrl": "https://mp.weixin.qq.com/s/6be6Ja7knvz7VLUj-Q262g"
        },
    }
}
``` 
### 1.5 返回参数
字段              |字段类型|字段说明
------------|-----------|-----------
errorCode       |int    |错误码(0：成功)
errorMsg        |string |错误说明
data            |object |一般为对象
|               |
data            |       |
introduction... |object |简介对象
|               |
introduction    |       |
name            |string |名称
imageUrl        |string |背景图片
toUrl           |string |跳转页面
### 1.6 错误状态码
参见 [全局响应状态码说明](../introduction.html/#134-全局响应状态码说明)


---